import getRedisClient from './redisClient';

export class FlavorSingleton {
	async _getFlavorMsgs(): Promise<FlavorMsgs> {
		const client = await getRedisClient();
		return await client.json.get("wtfix:msg") as FlavorMsgs ?? {};
	}

	async getXurMsg(): Promise<string> {
		const msgs = await this._getFlavorMsgs();
		return msgs.xurmsg ?? "";
	}

	async getPSA(): Promise<string> {
		const msgs = await this._getFlavorMsgs();
		return msgs.psa ?? "";
	}

	async getRiff(): Promise<string> {
		const msgs = await this._getFlavorMsgs();
		return msgs.riff ?? "";
	}
}

interface FlavorMsgs {
	xurmsg?: string
	psa?: string
	riff?: string
}

export const Flavor = new FlavorSingleton();
