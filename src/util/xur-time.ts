export const MORNING_RESET_HOUR = 17;
export const XUR_DAY_OF_WEEK = 5; // Friday
export const WEEKLY_RESET_DAY_OF_WEEK = 2; // Tuesday

export default function isItXurTime() {
    let time = new Date();
    let day = time.getUTCDay();
    let hour = time.getUTCHours();
  
    if (
      (WEEKLY_RESET_DAY_OF_WEEK < day && day < XUR_DAY_OF_WEEK) ||
      (day == WEEKLY_RESET_DAY_OF_WEEK && hour >= MORNING_RESET_HOUR) ||
      (day == XUR_DAY_OF_WEEK && hour < MORNING_RESET_HOUR)
    ) {
      return false;
    } else {
      return true;
    }
  }