import Image from "next/image";

import { DataBlock } from "@/components/data-block";
import { DestinyItem } from "@/components/destiny-items/destiny-item";
import { TextBlock } from "@/components/text-block";
import HunterIcon from "@/icons/hunter";
import TitanIcon from "@/icons/titan";
import WarlockIcon from "@/icons/warlock";
import { combineClasses } from "@/util/css";
import { Flavor } from "@/util/flavor";
import {
  Item,
  getInventoryItemDef,
  getXurInventory,
  getXurStatus,
} from "@/util/wtfixapi";
import Link from "next/link";
import { XurLocationStatus } from "@/util/xur";

export default async function Home() {
  let cats: GearCategory[] | undefined = undefined;
  let xurStatus: XurLocationStatus | undefined;
  let map: string = "/images/maps/unknown.png";
  let useMapFilters = false;
  try {
    xurStatus = await getXurStatus();

    if (xurStatus.present == false) throw "Xur is not present, giving up";

    //if (xurStatus?.location?.customMap) {
    //  map = xurStatus.location.customMap;
    //} else if (xurStatus?.location?.maps) {
    //  map = xurStatus.location.maps.site[xurStatus.location.maps.featured];
    //  useMapFilters = true;
    //}

    map = "/images/maps/doodle/new_tower_red.png"
    useMapFilters = true;

    // let xurInventory = await getXurInventory();
    
    cats = []

    // cats = [
    //   {
    //     exotics: [],
    //     legendaries: [],
    //     extras: [],
    //   },
    //   {
    //     exotics: [],
    //     legendaries: [],
    //     extras: [],
    //     symbol: <WarlockIcon />,
    //   },
    //   {
    //     exotics: [],
    //     legendaries: [],
    //     extras: [],
    //     symbol: <HunterIcon />,
    //   },
    //   {
    //     exotics: [],
    //     legendaries: [],
    //     extras: [],
    //     symbol: <TitanIcon />,
    //   },
    // ];

    // for (let item of xurInventory.weapons.shared) {
    //   let itemDef = await getInventoryItemDef(item.itemHash);
    //   if (itemDef.inventory?.tierType == 6) {
    //     cats[0].exotics.push(item);
    //   } else {
    //     cats[0].legendaries.push(item);
    //   }
    // }

    // cats[0].extras = xurInventory.armor.shared;

    // for (let item of xurInventory.armor.warlock) {
    //   let itemDef = await getInventoryItemDef(item.itemHash);
    //   if (itemDef.inventory?.tierType == 6) {
    //     cats[1].exotics.push(item);
    //   } else {
    //     cats[1].legendaries.push(item);
    //   }
    // }
    // cats[1].extras = xurInventory.weapons.warlock;

    // for (let item of xurInventory.armor.hunter) {
    //   let itemDef = await getInventoryItemDef(item.itemHash);
    //   if (itemDef.inventory?.tierType == 6) {
    //     cats[2].exotics.push(item);
    //   } else {
    //     cats[2].legendaries.push(item);
    //   }
    // }
    // cats[2].extras = xurInventory.weapons.hunter;

    // for (let item of xurInventory.armor.titan) {
    //   let itemDef = await getInventoryItemDef(item.itemHash);
    //   if (itemDef.inventory?.tierType == 6) {
    //     cats[3].exotics.push(item);
    //   } else {
    //     cats[3].legendaries.push(item);
    //   }
    // }
    // cats[3].extras = xurInventory.weapons.titan;
  } catch (e) {
    console.error(e);
  }

  return (
    <>
      {xurStatus?.present && (
        <div className="mb-8 flex flex-wrap justify-center gap-8">
          <DataBlock header="Map">
            <Image
              src={map}
              alt=""
              width={400}
              height={400}
              className="m-auto"
              style={{ filter: useMapFilters ? "var(--map-filter)" : "" }}
            />
          </DataBlock>
          {cats && (
            <DataBlock
              header="Xûr's Inventory"
              className="max-w-fit flex-1 sm:min-w-[30rem]"
            >
              <div className="flex flex-col gap-5">
                <h1>{"Gone raidin'"}</h1>
                <p>{"I'm currently trying to kill the witness and can't come to the phone right now, please check Xur in game to see what he's selling, the site will have his inventory next week."}</p>
              </div>
            </DataBlock>
          )}
        </div>
      )}
      <div className="flex flex-wrap justify-center gap-8">
        <TextBlock header="What The Fuck Is New?" className="w-96 max-w-full">
          <div className="flex flex-col gap-4">
            <ul className="ml-4 list-disc">
              <li>
                {
                  "Removed many pages and features that weren't used much, including the data page, guides, non-xur info on the homepage."
                }
              </li>
              <li>
                {
                  "Made Xur's location BIG on the top of every page, so it's always right there when you need it."
                }
              </li>
              <li>
                {
                  "Added all of Xur's wares to the homepage, with a new tooltip to read exactly the perks and stats the gear rolled with."
                }
              </li>
              <li>
                {"Added a countdown for when Xur comes and goes every week."}
              </li>
              <li>
                {
                  "Fixed up the themes (shaders) system we had and brought them front and center."
                }
              </li>
              <li>
                {
                  "Completely rewrote every piece of code that made the site tick."
                }
              </li>
            </ul>
          </div>
        </TextBlock>
        <TextBlock header="Discord" className="w-96 max-w-full">
          <div className="flex flex-col gap-4">
            <p>{"Yo.  We've got a Discord.  It's pretty cool (not biased)."}</p>
            <p>{"You should maybe join it or something idk."}</p>
            <Link
              href="https://discord.gg/VE4vz5z"
              className="text-3xl font-bold"
            >
              Official WTFIX Discord
            </Link>
          </div>
        </TextBlock>
        <TextBlock header="Community Xûrvice" className="w-96 max-w-full">
          <div
            className="flex flex-col gap-4"
            dangerouslySetInnerHTML={{ __html: await Flavor.getRiff() }}
          ></div>
        </TextBlock>
      </div>
    </>
  );
}

interface GearCategory {
  exotics: Item[];
  legendaries: Item[];
  extras: Item[];
  symbol?: JSX.Element;
}
