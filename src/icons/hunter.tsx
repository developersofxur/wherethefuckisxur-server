export default function HunterIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100%"
      height="100%"
      viewBox="0 0 32 32"
      {...props}
    >
      <path d="M9.055 10.446 16 10.423 9.052 20.874 16 20.85 8.588 32H1.543l7.036-10.428H1.543L8.575 11.15H1.543L9.05.024 16 0zm13.89 0L16 0l6.95.024 7.507 11.126h-7.032l7.032 10.422h-7.036L30.457 32h-7.045L16 20.85l6.948.024L16 10.423z" />
    </svg>
  )
}