import Image from "next/image";

import {
  DestinyInventoryItemDefinition,
  DestinyItemInstanceComponent,
  DestinyItemPerksComponent,
  DestinyItemPlugComponent,
  DestinyItemReusablePlugsComponent,
  DestinyItemSocketsComponent,
  DestinyItemStatsComponent,
  DestinyItemType,
  TierType,
} from "bungie-api-ts/destiny2";
import ItemTooltip from "./item-tooltip";
import { combineClasses } from "@/util/css";
import {
  getDamageTypeDef,
  getInventoryItemDef,
  getStatDef,
} from "@/util/wtfixapi";

export async function DestinyItem({
  itemHash,
  instance,
  stats,
  sockets,
  reusablePlugs,
}: {
  itemHash: number;
  instance: DestinyItemInstanceComponent;
  stats: DestinyItemStatsComponent;
  sockets: DestinyItemSocketsComponent;
  reusablePlugs: DestinyItemReusablePlugsComponent;
}) {
  const itemDef = await getInventoryItemDef(itemHash);

  let icon =
    itemDef.displayProperties.highResIcon || itemDef.displayProperties.icon;
  if (icon) {
    icon = "https://bungie.net" + icon;
  }

  let subtitle = "";
  if (itemDef.itemType == 2) {
    subtitle += itemDef.itemTypeDisplayName;
  } else if (itemDef.itemType == 3) {
    if (instance.damageTypeHash) {
      subtitle += `${(await getDamageTypeDef(instance.damageTypeHash)).displayProperties.name} `;
    }
    subtitle += itemDef.itemTypeDisplayName;
  }

  let headerBg = "";
  switch (itemDef.inventory?.tierType) {
    case 5:
      headerBg = "bg-purple-600";
      break;
    case 6:
      headerBg = "bg-yellow-400";
      break;
  }

  let armorStats;
  let statDefs: any = {};
  if (itemDef.itemType == 2 && stats) {
    for (const stat of Object.keys(stats.stats)) {
      statDefs[stats.stats[stat as unknown as number].statHash] =
        await getStatDef(stats.stats[stat as unknown as number].statHash);
    }
    armorStats = Object.values(stats.stats).sort(
      (a, b) => statDefs[a.statHash].index - statDefs[b.statHash].index,
    );
  }

  let intrinsic;
  const intrinsicSocketCategory = itemDef.sockets?.socketCategories.find((x) =>
    [3154740035, 3956125808].includes(x.socketCategoryHash),
  );
  if (intrinsicSocketCategory?.socketCategoryHash == 3956125808) {
    const intrinsicHash =
      intrinsicSocketCategory?.socketCategoryHash !== undefined
        ? itemDef.sockets?.socketEntries[
            intrinsicSocketCategory?.socketIndexes[0]
          ]?.singleInitialItemHash
        : undefined;
    intrinsic =
      intrinsicHash !== undefined
        ? await getInventoryItemDef(intrinsicHash)
        : undefined;
  } else if (intrinsicSocketCategory?.socketCategoryHash == 3154740035) {
    //Armor has its intrinsic in a category of multiple perks, gotta find the right one
    const intrinsicEntry = intrinsicSocketCategory?.socketIndexes?.find(
      (x) => itemDef.sockets?.socketEntries[x]?.socketTypeHash == 965959289,
    );
    const intrinsicHash =
      intrinsicEntry !== undefined
        ? itemDef.sockets?.socketEntries[intrinsicEntry]?.singleInitialItemHash
        : undefined;
    intrinsic =
      intrinsicHash !== undefined
        ? await getInventoryItemDef(intrinsicHash)
        : undefined;
  }

  const perks: DestinyInventoryItemDefinition[][] = [];
  const wepPerkSocketCategory = itemDef.sockets?.socketCategories.find(
    (x) => x.socketCategoryHash === 4241085061,
  );
  if (wepPerkSocketCategory) {
    for (const index of wepPerkSocketCategory.socketIndexes) {
      const socketEntry = itemDef.sockets?.socketEntries[index];
      if (socketEntry?.socketTypeHash === 1282012138) {
        continue;
      }
      const socketInstancePlugs = reusablePlugs?.plugs[index];
      if (socketInstancePlugs === undefined) {
        if (socketEntry?.singleInitialItemHash !== undefined) {
          perks.push([
            await getInventoryItemDef(socketEntry.singleInitialItemHash),
          ]);
        }
      } else {
        let plugs = [];
        for (const plug of socketInstancePlugs) {
          plugs.push(await getInventoryItemDef(plug.plugItemHash));
        }
        perks.push(plugs);
      }
    }
  }

  return (
    <ItemTooltip image={icon}>
      <div className=" bg-neutral-900 text-neutral-100">
        <div className={combineClasses(headerBg, "px-2")}>
          <h2 className="text-2xl font-semibold uppercase">
            {itemDef.displayProperties.name}
          </h2>
          <h3>{subtitle}</h3>
        </div>
        {armorStats && (
          <div className="p-2">
            <table className="border-separate border-spacing-x-1">
              <tbody>
                {armorStats.map((stat, i) => (
                  <tr key={i} className="leading-5">
                    <td className="w-24 text-right">
                      {statDefs[stat.statHash].displayProperties.name}
                    </td>
                    <td className="align-baseline">
                      <div className="inline-block w-40 bg-neutral-700">
                        <div
                          style={{
                            width: String((stat.value / 42) * 100) + "%",
                          }}
                          className="h-3 bg-neutral-100"
                        ></div>
                      </div>
                    </td>
                    <td>{stat.value}</td>
                  </tr>
                ))}
                <tr>
                  <td className="w-24 text-right">Total</td>
                  <td>
                    {armorStats
                      .map((x) => x.value)
                      .reduce((total, x) => total + x)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        )}
        {intrinsic && (
          <div className="flex items-start gap-2 bg-neutral-800 p-2">
            <Image
              src={"https://bungie.net" + intrinsic.displayProperties.icon}
              alt=""
              width={32}
              height={32}
            />
            <div>
              <p className="mb-1 leading-4">
                {intrinsic.displayProperties.name}
              </p>
              <p className="whitespace-pre-wrap text-sm leading-4">
                {intrinsic.displayProperties.description}
              </p>
            </div>
          </div>
        )}
        {perks.length > 0 && (
          <div className="mx-2">
            {perks.map(
              (x, i) =>
                x && (
                  <>
                    {i > 0 && <hr />}
                    <div key={i} className="flex flex-col gap-2 py-2">
                      {x.map(
                        (y, j) =>
                          y && (
                            <div key={j} className="flex items-start gap-2">
                              <Image
                                src={
                                  "https://bungie.net" +
                                  y.displayProperties.icon
                                }
                                alt=""
                                width={32}
                                height={32}
                              />
                              <div>
                                <p className="mb-1 leading-4">
                                  {y.displayProperties.name}
                                </p>
                                <p className="whitespace-pre-wrap text-sm leading-4">
                                  {y.displayProperties.description}
                                </p>
                              </div>
                            </div>
                          ),
                      )}
                    </div>
                  </>
                ),
            )}
          </div>
        )}
      </div>
    </ItemTooltip>
  );
}
