"use client";

import Image from 'next/image'

import { ThemeContext } from "@/app/contexts";
import { THEMES } from "@/util/themes";
import { Menu, Transition } from "@headlessui/react";
import { useContext } from "react";

export function ThemeSwitcher() {

  let [theme, setTheme] = useContext(ThemeContext);

  let themeObj = THEMES.find((item) => item.name == theme) ?? THEMES[0];

  return (
    <>
      <Menu as="div" className="relative">
        <Menu.Button className="hover:bg-gray-400/25 w-8">
          <Image src={themeObj.icon} alt={themeObj.name} width={100} height={100}/>
        </Menu.Button>
        <Transition
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute top-0 right-0 rounded-md theme-main-bg shadow-lg border-2 border-solid border-[--border-color] origin-bottom-left w-16">
            {THEMES.map((item, i) => (
              <Menu.Item key={i} as="div" className="m-1 p-1 rounded-md hover:bg-gray-500/10" onClick={() => { setTheme(item.name) }}>
                <Image src={item.icon} alt={item.name} width={100} height={100}/>
              </Menu.Item>
            ))}
          </Menu.Items>
        </Transition>
      </Menu>

    </>
  )
}