import { combineClasses } from "@/util/css"

export function DataBlock({header, className, children}: {header: string, className?: string, children: React.ReactNode}) {
  return (
    <div className={combineClasses(className, "p-5 theme-block-bg border border-[--border-color]")}>
      <h2 className="text-3xl text-center font-bold uppercase mb-4">{header}</h2>
      <div className="text-[--special-text-color]">
        {children}
      </div>
    </div>
  )
}